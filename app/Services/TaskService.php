<?php

namespace App\Services;

use App\Interfaces\Data\TaskInterface;
use App\Interfaces\Services\TaskServiceInterface;
use App\Models\Task;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class TaskService implements TaskServiceInterface
{
    private TaskInterface $task;

    public function __construct(TaskInterface $task)
    {
        $this->task = $task;
    }

    public function getAllTasks(): LengthAwarePaginator
    {
        return $this->task::query()->paginate(10);
    }

    public function getTaskById(int $id): ?TaskInterface
    {
        return $this->task::all()->find($id);
    }

    public function createTask(Request $request): TaskInterface
    {
        return Task::factory()->create($request->all());
    }

    public function updateTaskById(Request $request, int $id): bool
    {
        $task = $this->getTaskById($id);

        if (isset($task)) {
            $task->fill($request->all());
            return $task->save();
        }

        return false;
    }

    public function deleteTaskById(int $id): ?bool
    {
        $task = $this->getTaskById($id);

        if (isset($task)) {
            return $task->delete();
        }

        return null;
    }
}
