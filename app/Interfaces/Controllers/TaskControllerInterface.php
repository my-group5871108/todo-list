<?php

namespace App\Interfaces\Controllers;

use App\Http\Requests\TaskStoreRequest;
use App\Interfaces\Services\TaskServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * @OA\Info(
 *     title="ToDo list API documentation",
 *     version="1.0.0",
 *     @OA\Contact(
 *         email="strizhenok95@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 * @OA\Tag(
 *     name="ToDo",
 *     description="ToDo list",
 * )
 * @OA\Server(
 *     description="Laravel Swagger API server",
 *     url="http://127.0.0.1:8000/api"
 * )
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     name="X-APP-ID",
 *     securityScheme="X-APP-ID"
 * )
 */

interface TaskControllerInterface
{
    public function __construct(TaskServiceInterface $taskService);

    /**
     * @OA\Get(
     *     path="/tasks",
     *     operationId="tasksAll",
     *     tags={"ToDo"},
     *     summary="Display a listing of the tasks",
     *     security={
     *       {"api_key": {}},
     *     },
     *     @OA\Parameter(
     *         name="page",
     *         in="query",
     *         description="The page number",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Tasks received successfully",
     *     ),
     * )
     *
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse;

    /**
     * @OA\Get(
     *     path="/tasks/{id}",
     *     operationId="tasksShow",
     *     tags={"ToDo"},
     *     summary="Get task by ID",
     *     security={
     *       {"api_key": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of task",
     *         required=true,
     *         example="1",
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Task received successfully"
     *     ),
     *     @OA\Response(
     *          response="404",
     *          description="The task does not exist"
     *      ),
     * )
     *
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse;

    /**
     * @OA\Post(
     *     path="/tasks",
     *     operationId="tasksCreate",
     *     tags={"ToDo"},
     *     summary="Create task",
     *     security={
     *       {"api_key": {}},
     *     },
     *     @OA\RequestBody(
     *          @OA\JsonContent(ref="#/components/schemas/TaskStoreRequest")
     *     ),
     *     @OA\Response(
     *         response="202",
     *         description="Task created successfully"
     *     )
     * )
     * Store a newly created resource in storage.
     *
     * @param TaskStoreRequest $request
     * @return JsonResponse
     */
    public function store(TaskStoreRequest $request): JsonResponse;

    /**
     * @OA\Put(
     *     path="/tasks/{id}",
     *     operationId="tasksUpdate",
     *     tags={"ToDo"},
     *     summary="Update task by ID",
     *     security={
     *       {"api_key": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of task",
     *         required=true,
     *         example="1",
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *         @OA\JsonContent(ref="#/components/schemas/TaskStoreRequest")
     *      ),
     *     @OA\Response(
     *         response="200",
     *         description="Task changed",
     *     ),
     *     @OA\Response(
     *           response="404",
     *           description="The task does not exist"
     *     ),
     * )
     *
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse;

    /**
     * @OA\Delete(
     *     path="/tasks/{id}",
     *     operationId="tasksDelete",
     *     tags={"ToDo"},
     *     summary="Delete task by ID",
     *     security={
     *       {"api_key": {}},
     *     },
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="The ID of task",
     *         required=true,
     *         example="1",
     *         @OA\Schema(
     *             type="integer",
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Deleted",
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="The task does not exist"
     *     ),
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse;
}
