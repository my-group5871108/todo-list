<?php

namespace App\Interfaces\Services;

use App\Interfaces\Data\TaskInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface TaskServiceInterface
{
    public function getAllTasks(): LengthAwarePaginator;

    public function getTaskById(int $id): ?TaskInterface;

    public function createTask(Request $request): TaskInterface;

    public function updateTaskById(Request $request, int $id): bool;

    public function deleteTaskById(int $id): ?bool;
}
