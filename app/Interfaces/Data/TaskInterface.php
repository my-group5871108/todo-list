<?php

namespace App\Interfaces\Data;

interface TaskInterface
{
    const TITLE_KEY = 'title';
    const CONTENT_KEY = 'content';
    const IS_DONE_KEY = 'is_done';
}
