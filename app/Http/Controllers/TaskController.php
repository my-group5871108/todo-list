<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskStoreRequest;
use App\Interfaces\Controllers\TaskControllerInterface;
use App\Interfaces\Services\TaskServiceInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class TaskController extends Controller implements TaskControllerInterface
{
    private TaskServiceInterface $taskService;

    public function __construct(TaskServiceInterface $taskService)
    {
        $this->taskService = $taskService;
    }

    public function index(): JsonResponse
    {
        $tasks = $this->taskService->getAllTasks();
        return response()->json($tasks);
    }

    public function show(int $id): JsonResponse
    {
        $task = $this->taskService->getTaskById($id);

        if (isset($task)) {
            return response()->json($task);
        }

        return response()->json([
            'message' => 'The task does not exist'
        ], 404);
    }

    public function store(TaskStoreRequest $request): JsonResponse
    {
        $task = $this->taskService->createTask($request);

        return response()->json($task, 202);
    }

    public function update(Request $request, int $id): JsonResponse
    {
        $status = $this->taskService->updateTaskById($request, $id);

        if ($status) {
            return response()->json($status);
        }

        return response()->json([
            'message' => 'The task does not exist'
        ], 404);
    }

    public function destroy(int $id): JsonResponse
    {
        $status = $this->taskService->deleteTaskById($id);

        if ($status) {
            return response()->json($status);
        }

        return response()->json([
            'message' => 'The task does not exist'
        ], 404);
    }
}
