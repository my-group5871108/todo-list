<?php

namespace App\Providers;

use App\Http\Controllers\TaskController;
use App\Interfaces\Data\TaskInterface;
use App\Models\Task;
use App\Services\TaskService;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\Services\TaskServiceInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->when(TaskService::class)
            ->needs(TaskInterface::class)
            ->give(Task::class);

        $this->app->when(TaskController::class)
            ->needs(TaskServiceInterface::class)
            ->give(TaskService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
