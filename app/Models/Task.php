<?php

namespace App\Models;

use App\Interfaces\Data\TaskInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model implements TaskInterface
{
    use HasFactory;

    protected $table = 'tasks';
    protected $fillable = ['title', 'content', 'is_done'];
}
