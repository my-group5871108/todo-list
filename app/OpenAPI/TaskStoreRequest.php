<?php

namespace App\OpenAPI;

/**
 * @OA\Schema(
 *     type="object",
 *     title="Task store request",
 *     description="Request for create task",
 * )
 */
class TaskStoreRequest
{
    /**
     * @OA\Property(
     *     title="Title",
     *     description="Task title",
     *     example="Task title",
     * )
     *
     * @var string
     */
    public string $title;

    /**
     * @OA\Property(
     *     title="Content",
     *     description="Task content",
     *     example="Task content",
     * )
     *
     * @var string
     */
    public string $content;

    /**
     * @OA\Property(
     *     title="IsDone",
     *     description="Task status",
     *     example=true,
     * )
     *
     * @var bool
     */
    public bool $is_done;
}
