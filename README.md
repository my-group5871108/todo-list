PHP 8.1

## Seed
Заполнение таблицы tasks дефолтными данными - `php artisan migrate:fresh --seed`

## Test
Запуск тестов - `php artisan test`

## OpenApi
Генерация документации OpenAPI - `php artisan l5-swagger:generate `
После генерации её можно просмотреть по ссылке - http://127.0.0.1:8000/api/documentation
