<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class TaskApiTest extends TestCase
{
    use DatabaseMigrations;

    public function testTaskIndex(): void
    {
        Task::factory(10)->create();
        $response = $this->get('/api/tasks');

        $response
            ->assertOk()
            ->assertJson([]);
    }

    public function testTaskShow(): void
    {
        $this->createDefaultTask();
        $response = $this->get('/api/tasks/1');

        $response
            ->assertOk()
            ->assertJson(fn(AssertableJson $json) => $json
                ->where('id', 1)
                ->where('title', 'Test title')
                ->where('content', 'Test content')
                ->where('is_done', 1)
                ->etc()
            );
    }

    public function testTaskStore(): void
    {
        $response = $this->post('/api/tasks', [
            'id' => '1',
            'title' => 'Test title',
            'content' => 'Test content',
            'is_done' => true
        ]);

        $response
            ->assertStatus(202)
            ->assertJson(fn(AssertableJson $json) => $json
                ->where('id', 1)
                ->where('title', 'Test title')
                ->where('content', 'Test content')
                ->where('is_done', true)
                ->etc()
            );

        $this
            ->assertDatabaseCount('tasks', 1)
            ->assertDatabaseHas('tasks', [
                'id' => 1,
                'title' => 'Test title',
                'content' => 'Test content',
                'is_done' => true
            ]);
    }

    public function testTaskUpdate(): void
    {
        $this->createDefaultTask();

        $response = $this->put('/api/tasks/1', [
            'title' => 'Test title update',
            'content' => 'Test content update',
            'is_done' => false
        ]);

        $response
            ->assertOk();

        $this
            ->assertDatabaseCount('tasks', 1)
            ->assertDatabaseHas('tasks', [
                'id' => 1,
                'title' => 'Test title update',
                'content' => 'Test content update',
                'is_done' => false
            ]);
    }

    public function testTaskDestory(): void
    {
        $this->createDefaultTask();

        $this->assertDatabaseHas('tasks', [
            'id' => 1,
            'title' => 'Test title',
            'content' => 'Test content',
            'is_done' => true
        ]);

        $response = $this->delete('/api/tasks/1');
        $response->assertOk();

        $this->assertDatabaseMissing('tasks', [
            'id' => 1
        ]);
    }

    private function createDefaultTask(): void
    {
        Task::factory()->create([
            'id' => '1',
            'title' => 'Test title',
            'content' => 'Test content',
            'is_done' => true
        ]);
    }
}
